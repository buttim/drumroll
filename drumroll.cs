//!csc -nologo -debug+ -t:winexe "%file%" -win32icon:drum.ico -res:slidewhistle.png -res:snare.png -res:cymbals.png -res:drumcymbals.png -res:drum.ico -res:drumroll.wav -res:cymbals.wav -res:slide.wav -res:drumcymbals.wav -res:trombone.wav -res:trombone.png -res:boing.wav -res:spring.png
using System;
using System.Media;
using System.Drawing;
using System.Windows.Forms;

class App : Form {
	static SoundPlayer player;
	Button btnDrum=new Button(),
		btnCymbals=new Button(),
		btnSlide=new Button(),
		btnDrumCymbals=new Button(),
		btnTrombone=new Button(),
		btnBoing=new Button();
	static void PlaySound(string name) {
		PlaySound(name,false);
	}
	static void PlaySound(string name,bool looping) {
		player=new SoundPlayer(typeof(App).Assembly.GetManifestResourceStream(name+".wav"));
		player.Load();
		if (looping)
			player.PlayLooping();
		else
			player.Play();
	}
	App() {
		Text="Drum roll";
		Size=new Size(840,530);
		FormBorderStyle=FormBorderStyle.FixedDialog;
		MinimizeBox=MaximizeBox=false;
		Icon=new Icon(GetType().Assembly.GetManifestResourceStream("drum.ico"));

		btnDrum.Text="&Rullo";
		btnDrum.TextImageRelation=TextImageRelation.ImageAboveText;
		btnDrum.Image=new Bitmap(GetType().Assembly.GetManifestResourceStream("snare.png"));
		btnDrum.Size=new Size(250,250);
		btnDrum.Location=new Point(20,10);
		btnDrum.Click+= (o,e)=> { PlaySound("drumroll",true); };

		btnCymbals.Text="&Piatti";
		btnCymbals.TextImageRelation=TextImageRelation.ImageAboveText;
		btnCymbals.Image=new Bitmap(GetType().Assembly.GetManifestResourceStream("cymbals.png"));
		btnCymbals.Size=new Size(250,250);
		btnCymbals.Location=new Point(290,10);
		btnCymbals.Click+= (o,e)=> { PlaySound("cymbals"); };

		btnSlide.Text="&Coulisse";
		btnSlide.TextImageRelation=TextImageRelation.ImageAboveText;
		btnSlide.Image=new Bitmap(GetType().Assembly.GetManifestResourceStream("slidewhistle.png"));
		btnSlide.Size=new Size(250,250);
		btnSlide.Location=new Point(560,10);
		btnSlide.Click+= (o,e)=> { PlaySound("slide"); };
		
		btnDrumCymbals.Text="&Baddum Tish!";
		btnDrumCymbals.TextImageRelation=TextImageRelation.ImageAboveText;
		btnDrumCymbals.Image=new Bitmap(GetType().Assembly.GetManifestResourceStream("drumcymbals.png"));
		btnDrumCymbals.Size=new Size(250,200);
		btnDrumCymbals.Location=new Point(20,280);
		btnDrumCymbals.Click+= (o,e)=> { PlaySound("drumcymbals"); };
		
		btnTrombone.Text="&Wah wah wah waaaah";
		btnTrombone.TextImageRelation=TextImageRelation.ImageAboveText;
		btnTrombone.Image=new Bitmap(GetType().Assembly.GetManifestResourceStream("trombone.png"));
		btnTrombone.Size=new Size(250,200);
		btnTrombone.Location=new Point(290,280);
		btnTrombone.Click+= (o,e)=> { PlaySound("trombone"); };
		
		btnBoing.Text="Boin&g";
		btnBoing.TextImageRelation=TextImageRelation.ImageAboveText;
		btnBoing.Image=new Bitmap(GetType().Assembly.GetManifestResourceStream("spring.png"));
		btnBoing.Size=new Size(250,200);
		btnBoing.Location=new Point(560,280);
		btnBoing.Click+= (o,e)=> {PlaySound("boing"); };
		
		Controls.AddRange(new Control[]{btnDrum,btnCymbals,btnSlide,btnDrumCymbals,btnTrombone,btnBoing});
	}
	[STAThread]
	static void Main() {
		Application.EnableVisualStyles();
		Application.Run(new App());
	}
}